<?php

return [
    'title'       => 'SecretSanta.fr - Verschenken Sie Geschenke... geheim!',
    'description' => 'SecretSanta.fr ist ein einfaches, kostenloses und ohne Registrierung nutzbares Tool, das bei der Organisation eines geheimen Geschenkaustauschs unter Freunden oder Kollegen hilft (bekannt als Secret Santa).',
    'keywords'    => 'secret santa,party,organisation,secretsanta,weihnachten,geschenk,geheim,kostenlos,easy,registrierung,open,santa,secret-santa,kollegen,freunde,anonym,zufällig,random,ziehung,lotterie,geschenketausch,tausch,geschenke,weihnachtsmann,weihnachtsfrau',
    'logo_alt'    => 'Wer wird Ihr Secret Santa sein?',
];
