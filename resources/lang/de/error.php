<?php

return [
    'validation' => 'Die eingegebenen Daten sind ungültig.',
    'solution'   => 'Die von Ihnen gewählten Ausschlüsse erlauben es nicht, jedem Teilnehmer ein Ziel zuzuweisen.',
    'csrf'       => 'Das Formular ist aufgrund von Inaktivität abgelaufen. Bitte versuchen Sie es erneut.',
    'signature'  => 'Die angeforderte Aktion konnte nicht ausgeführt werden. Bitte laden Sie die Seite neu und versuchen Sie es erneut.',
    'email'      => 'Beim Versand der E-Mail ist ein Fehler aufgetreten. Bitte versuchen Sie es später noch einmal.',
    'internal'   => 'Es ist ein interner Fehler aufgetreten. Bitte versuchen Sie es später noch einmal.',
    'resend'     => 'Sie müssen warten, bevor Sie diese E-Mail erneut senden können.',
    'withdraw'   => 'Es können nicht weniger als 3 Teilnehmer vorhanden sein.',
    'expired'    => 'Diese Veranstaltung ist noch nicht beendet.',
    'solvable'   => 'Diese Veranstaltung erlaubt diese Generierung nicht.',
    'fixOrganizer' => [
        'drawNotFoundOrExpired' => 'Die Ziehung, die mit dieser Adresse verknüpft ist, existiert nicht oder ist bereits beendet.',
        'distanceEmailTooBig' => 'Die eingegebene E-Mail-Adresse weicht zu stark von der ursprünglichen ab. Bitte kontaktieren Sie den Entwickler der Website.',
        'resendDelayTooShort' => 'Sie haben diese E-Mail-Adresse kürzlich bereits geändert. Bitte warten Sie etwas länger.'
    ]
];
