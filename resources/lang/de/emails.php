<?php

return [
    'organizer.content' => 'Um Ihr Organisationsschild bis zum :expiration einzusehen, können Sie folgende Seite aufrufen: :link',
    'organizer_recap_title'   => 'SecretSanta #:draw - Zusammenfassung Organisator/in',
    'organizer_final_recap_title' => 'SecretSanta #:draw - Finale Zusammenfassung Organisator/in',
    'target_draw.title' => 'SecretSanta #:draw - :subject',
    'dear_santa.title'  => 'SecretSanta #:draw - Nachricht des Empfängers Ihres Geschenks',
    'target_withdrawn.title' => 'SecretSanta #:draw - Rücktritt des Empfängers von Ihrem Geschenk',
];
