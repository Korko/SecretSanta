<?php

return [
    'reminder' => "Denken Sie daran, dass Ihr geheimer Weihnachtsmann oder Ihre geheime Weihnachtsfrau Ihnen nicht antworten kann! Geben sie ihm oder ihr so viele Informationen wie möglich zu geben.",
    'list'    => [
        'date'    => 'Versanddatum',
        'body'    => 'Nachrichtentext',
        'status'  => 'Empfangsstatus',
        'empty'   => 'Es wurden noch keine E-Mails gesendet',
        'caption' => 'Liste der E-Mails, die an den Weihnachtsmann/die Weihnachtsfrau gesendet wurden',
    ],
    'content' => [
        'label'       => 'E-Mail-Inhalt',
        'placeholder' => 'Lieber Weihnachtsmensch...',
    ],
    'resend' => [
        'button'  => 'Sende mir die E-Mails, die ich von meinem Empfänger erhalten habe, erneut.',
    ]
];
