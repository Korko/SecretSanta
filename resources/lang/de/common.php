<?php

return [
    'internal' => 'Ein Fehler ist aufgetreten',
    'success'  => 'Erfolg',

    'copied' => 'In die Zwischenablage kopiert',

    'fetcher' => [
        'load'    => 'Laden',
        'loading' => 'Ladevorgang läuft...',
    ],

    'form' => [
        'send'    => 'Senden',
        'sending' => 'Wird versendet...',
        'sent'    => 'Gesendet',
        'reset'   => 'Eine neue Ziehung beginnen'
    ],

    'modal' => [
        'close' => 'Schließen',
    ],

    'email' => [
        'redo' => 'Erneut senden',
        'status' => [
            'created'  => 'Warten auf die Versendung',
            'sending'  => 'Senden in Bearbeitung',
            'sent'     => 'Gesendet',
            'error'    => 'Fehler',
            'received' => 'Empfangen',
        ],
        'recent' => 'Sie können dieselbe E-Mail nicht zu schnell erneut versenden',
    ],
];
