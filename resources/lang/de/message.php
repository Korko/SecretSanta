<?php

return [
    'sent' => 'Erfolgreich gesendet!',
    'fixedOrganizer' => 'Adresse korrigiert, denken Sie daran, Ihre E-Mails zu überprüfen!',
];
