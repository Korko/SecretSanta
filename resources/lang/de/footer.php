<?php

return [
    'fork'    => 'Quellcode verfügbar auf :git',
    'version' => 'Aktuelle Version: :version',
    'project' => 'Projekt von :author',
    'theme'   => 'Design von :author',
    'icons'   => 'Symbole von :author',
    'legal'   => 'Impressum und Datenschutzrichtlinie',
];