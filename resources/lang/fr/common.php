<?php

return [
    'internal' => 'Une erreur est survenue',
    'success'  => 'Succès',

    'copied' => 'Copié dans le presse papier',

    'fetcher' => [
        'load'    => 'Charger',
        'loading' => 'Chargement en cours...',
    ],

    'form' => [
        'send'    => 'Envoyer',
        'sending' => 'Envoi en cours',
        'sent'    => 'Envoyé',
        'reset'   => 'Commencer un nouveau tirage'
    ],

    'modal' => [
        'close' => 'Fermer',
    ],

    'email' => [
        'redo' => 'Ré-envoyer',
        'status' => [
            'created'  => 'En attente d\'envoi',
            'sending'  => 'Envoi en cours',
            'sent'     => 'Envoyé',
            'error'    => 'Erreur',
            'received' => 'Reçu',
        ],
        'recent' => 'Vous ne pouvez pas relancer un même email trop rapidement',
    ],
];
