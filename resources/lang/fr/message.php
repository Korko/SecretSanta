<?php

return [
    'sent' => 'Envoyé avec succès !',
    'fixedOrganizer' => 'Adresse corrigée, pensez à consulter vos emails !',
];
