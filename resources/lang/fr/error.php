<?php

return [
    'validation' => 'Les données remplies ne sont pas valides',
    'solution'   => 'Les exclusions que vous avez choisies ne permettent pas d\'attribuer une cible par participant.',
    'csrf'       => 'Le formulaire a expiré pour cause d\'inactitivé. Veuillez recommencer.',
    'signature'  => 'L\'action demandée n\'a pas pu être effectuée. Veuillez rafraichir la page avant de retenter.',
    'email'      => 'Une erreur est survenue dans l\'envoi d\'email. Veuillez réessayer plus tard.',
    'internal'   => 'Une erreur interne est survenue. Veuillez réessayer plus tard.',
    'resend'     => 'Vous devez attendre avant de réenvoyer cet email.',
    'withdraw'   => 'Vous ne pouvez pas avoir moins de 3 participants.',
    'expired'    => 'Cet évènement n\'est pas encore terminé',
    'solvable'   => 'Cet évènement ne permet pas cette génération',
    'fixOrganizer' => [
        'drawNotFoundOrExpired' => 'Le tirage au sort associé à cette adresse n\'existe pas ou est terminé.',
        'distanceEmailTooBig' => 'L\'adresse email entrée est trop différente de celle d\'origine, contactez le développeur du site.',
        'resendDelayTooShort' => 'Vous avez déjà modifié cette adresse email il y a peu de temps. Merci de patienter un peu.'
    ]
];
