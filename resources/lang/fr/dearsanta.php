<?php

return [
    'reminder' => "Rappelez vous que votre Père/Mère Noël secrêt ne pourra pas vous répondre ! Pensez bien à lui donner le maximum d'informations possibles.",
    'list'    => [
        'date'    => 'Date d\'envoi',
        'body'    => 'Corps du message',
        'status'  => 'Status de réception de l\'email',
        'empty'   => 'Aucun email envoyé pour le moment',
        'caption' => 'Liste des emails envoyés au/à la Père/Mère Noël',
    ],
    'content' => [
        'label'       => 'Contenu du mail',
        'placeholder' => 'Cher Papa Noël...',
    ],
    'resend' => [
        'button'  => 'Me ré-envoyer les mails que j\'ai reçu de ma cible',
    ]
];
