@if (config('app.bmc'))
<a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/{{ config('app.bmc') }}" rel="noopener noreferrer">
    Offrez moi un café
</a>
@endif
